# Size 1

## Size 2

### Size 3

#### Size 4

##### Size 5

###### Size 6

>[!NOTE]
> a note

>[!TIP]
> a tip

>[!CAUTION]
> caution

>[!WARNING]
> a warning

>[!IMPORTANT]
> important
>
> _Italic_

>[!ADMIN]
>Admin

>[!AVAILABILITY]
>Availability 

>[!PREREQUISITES]
>Prerequisites

_**Bold and Italic**_

> [!NOTE]
> Table

| Column A text | Column B text | Column C text |
| ------------- | ------------- | ------------- |
| A1 text       | B1 text       | C1 text       |
| A1a           | B1a           | C1a           |
| A2            | B2            | C2            |
| A3            | B3            | C3            |

> [!NOTE]
> Table with header

| Column A | Column B | Column C |
| -------- | -------- | -------- |
| A1       | B1       | C1       |
| A2       | B2       | C2       |
| A3       | B3       | C3       |

- bullet points 1/3
- bullet points 2/3
- bullet points 3/3

1. Number one
1. Number two
1. Number five
1. Number six

- [ ] List item 1
- [ ] List item 2
- [ ] List item 3

> citation

| Column A | Column B | Column C |
| -------- | -------- | -------- |
| A1       | B1       | C1       |
| A2       | B2       | C2       |
| A3       | B3       | C3       |

| Column A | Column B | Column C |
| -------- | -------- | -------- |
| A1       | B1       | C1       |
| A2       | B2       | C2       |
| A3       | B3       | C3       |

```
Code Block
```

`Inline Code`

> Citations

**Bold**

~~Strikethrough~~

![Image](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Windows_logo_-_1992.svg/1200px-Windows_logo_-_1992.svg.png)

>[!NOTE]
> Another Note

![Hyperlink Text](https://img2.pngio.com/link-icon-png-link-icon-png-transparent-free-for-download-on-link-symbol-png-1600_1600.png)

## Localization Control

>[!WARNING]
>
> Here is a warning

>[!MORELIKETHIS]
>
> - bullet 1
> - bullet 2

### DNL

Here is a paragraph that has [!DNL Do not Localize] in it. You should see just the phrase "Do not Localize", with the !DNL markdown removed.

Here is a paragraph that has [DNL] without the exclamation mark. You should see a linter error for it.

Here is a paragraph that has [!DNL] without any content text. You should see a linter error for it.

Someone is allergic to the shift key - since this [!dnl wrong case] looks like a lazy typer at work.

### UICONTROL

Here is a paragraph that contains [!UICONTROL The UI Control Tag]. Just like DNL, for preview,this should just strip out the markdown and leave the plain text "The UI Control Tag".

Why would someone leave out the [UICONTROL] exclamation mark from this? It's just wrong.

This paragraph has [!UICONTROL] without any content. It should trigger a linter error.

The lazy typer returns by keeping [!uiControl] in the wrong case. Silly camel!

## Linter Exceptions

Here are some items that cause exceptions by default in MarkdownLint, but are ignored by Adobe Markdown Authoring.

MD009 - no trailing spaces. This line has trailing spaces.   
MD047 - files should end with newline. Look at the end of the file.
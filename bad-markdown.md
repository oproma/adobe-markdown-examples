---
title: Some Title that goes here
---

# Here is the Header. There should be only one of these

## This header is okay, except for the period.

#### This header is too deep! h4 needs to follow an h3, not an h2.

## Now, this is too shallow, but not really

### But this one is okay, as long

# This should actually be flagged. Because it is second  header. 

> [!NOTE]
>
>Okay - here is a note. It's formatted badly because there is a space before the [!NOTE]

>[!NOTE]
>
>This note is corrent and should not cause problems.

## Here is a table

| Column A | Column B | Column C |
| -------- | -------- | -------- |
| A1       | B1       | C1       |
| A2       | B2       | C2       |
| A3       | B3       | C3       |

>[!VIDEO](https://youtu.be/-EJ-4koV7m0)

>[!MORELIKETHIS] 
>
>
>- [Adobe Home](https://www.adobe.com)
>- [Experience League](https://experienceleague.adobe.com)


## Javascript Code Block

```javascript
function helloWorld(message = "World") {
  console.log(`Hello, ${message}`);
}
```

## Examples

Here are some examples of the Adobe extensions in action. If you have the extension installed, it will format this document in the built-in VSCode preview panel. If you do not have the extension installed, the generic VSCode Markdown Preview will use the CommonMark spec to render the alert extensions as block quotes.

### Note

```markdown
>[!NOTE]
>
> Here is a note component. Notice that it is just a blockquote that has a [!NOTE] label at the beginning of the code.
```

>[!NOTE]
>
> Here is a note component. Notice that it is just a blockquote that has a [!NOTE] label at the beginning of the code.

### Caution

```markdown
>[!CAUTION]
>
>Here is a caution component. Notice that it is just a blockquote and that you can _embed_ inline markdown including `pre-formatted text` and other **chicanery**
```

>[!CAUTION]
>
>Here is a caution component. Notice that it is just a blockquote and that you can _embed_ inline markdown including `pre-formatted text` and other **chicanery**

>[!TIP]
>
>Here is a tip [!TIP] This is after this.

### Important

```markdown
>[!IMPORTANT]
>
>Here is the _IMPORTANT_ component. It's only one line.
```

>[!IMPORTANT]
>
>Here is the _IMPORTANT_ component. It's only one line.

### Video

>[!VIDEO](https://youtube.com?watch="xyxz")

## MoreLikeThis

>[!MORELIKETHIS]
>
>- [Adobe Experience League](https://experienceleague.adobe.com)
>- [Markdown-It Extension](https://github.com/markdown-it/markdown-it)
>- [Microsoft Docs Authoring Extension](https://docs.microsoft.com/en-us/contribute/how-to-write-docs-auth-pack)

